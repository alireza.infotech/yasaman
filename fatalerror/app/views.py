from django.shortcuts import render, redirect
from .models import *


def index(requests):
    if requests.method == 'GET':
        print(len(Question.objects.all()[:10]))
        return render(requests, 'index.html', {
            'questions' : Question.objects.all()[:10]
        })
    else:
        return redirect('/error')
